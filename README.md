# Media Title Length

The Media Title Length allows you to control the media name field length.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_title_length).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_title_length).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

1. Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

2. Navigate to Administration > Extend. Ande enable Media title length module.
3. Navigate to Administration > Configuration > Media > Media title length settings.
Set and control your media title length.


## Configuration

After the installation:

1. Navigate to
Administration >> configuration >> media >> media title length settings

2. Set media title length.

3. Save & increase title length.
4. Run this command.
>> describe media_field_data;

The name field type length must be changed
>> name                          | varchar(1000)


## Maintainers

- Mohammad Fayoumi - [Mohammad-Fayoumi](https://www.drupal.org/u/mohammad-fayoumi)

- Development sponsored by Vardot - [Vardot](https://www.drupal.org/vardot)
