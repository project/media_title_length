<?php

namespace Drupal\media_title_length\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TitleLengthForm. The config form for the media_title_length module.
 *
 * @package Drupal\media_title_length\Form
 */
class TitleLengthForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_title_length_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'media_title_length.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title_length'] = [
      '#type' => 'number',
      '#title' => 'Media name length',
      '#description' => $this->t('Set the media name length that you prefer.'),
      '#default_value' => $this->config('media_title_length.config')
        ->get('title_length'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate form input value.
    $values = $form_state->getValues();
    if (!is_numeric($values['title_length'])) {
      $form_state->setErrorByName('title_length', $this->t('This should be an integer number.'));
    }

    // Check if the number is between 1 and 65535.
    if ($values['title_length'] <= 0 || $values['title_length'] > 65535) {
      $form_state->setErrorByName('title_length', $this->t('The number should be between 1 & 65,535.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('media_title_length.config');

    if (is_numeric($values['title_length'])) {
      // Change name length in database.
      media_title_length_changer('media', 'name', $values['title_length']);
      $config->set('title_length', $values['title_length'])
        ->set('entity_name', 'media')
        ->save();
    }

    $this->messenger()->addMessage($this->t('The media title length has been updated.'));
  }

}
